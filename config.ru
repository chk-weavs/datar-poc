require 'rack-livereload'
require 'sinatra'
require 'csv'
require 'json'

class MyApp < Sinatra::Application
  set :bind, '0.0.0.0'

  configure do
    enable :cross_origin
  end

  before do
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers["Allow"] = "GET, PUT, POST, DELETE, OPTIONS"
  end

  get '/' do
    @name = 1234
    erb :index
  end

  get '/frequency' do
    @mtb_paths = CSV.read("data/mtb-strecken.csv", headers: true).map(&:to_h)
    erb :frequency
  end


  get '/monstas/:name' do
    @name = params["name"]
    erb :monstas
  end


  options "*" do
    response.headers["Allow"] = "GET, PUT, POST, DELETE, OPTIONS"
    response.headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type, Accept, X-User-Email, X-Auth-Token"
    response.headers["Access-Control-Allow-Origin"] = "*"
    200
  end


end
run MyApp